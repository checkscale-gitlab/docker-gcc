# Copyright 2019 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ARG ALPINE_VERSION=latest
FROM alpine:${ALPINE_VERSION} as alpine

FROM alpine as gcc-builder

ARG GCC_VERSION=9.2.0

# Download, configure, build and install the GNU GCC C/C++ compiler
RUN apk add --quiet --no-cache \
        build-base \
        dejagnu \
        isl-dev \
        make \
        mpc1-dev \
        mpfr-dev \
        texinfo \
        zlib-dev && \
    wget \
        --quiet \
        https://ftp.gnu.org/gnu/gcc/gcc-${GCC_VERSION}/gcc-${GCC_VERSION}.tar.gz && \
    tar \
        --extract \
        --gzip \
        --file=gcc-${GCC_VERSION}.tar.gz && \
    \
    mkdir /gcc-${GCC_VERSION}/build-gcc && \
    cd /gcc-${GCC_VERSION}/build-gcc && \
    ../configure \
        --prefix=/usr/local \
        --build=$(uname -m)-alpine-linux-musl \
        --host=$(uname -m)-alpine-linux-musl \
        --target=$(uname -m)-alpine-linux-musl \
        --with-pkgversion="Alpine ${GCC_VERSION}" \
        --disable-nls \
        --disable-werror \
        --disable-libssp \
        --disable-libmpx \
        --disable-static \
        --disable-symvers \
        --disable-multilib \
        --disable-libmudflap \
        --disable-fixed-point \
        --disable-libsanitizer \
        --disable-libstdcxx-pch \
        --enable-tls \
        --enable-shared \
        --enable-default-pie \
        --enable-clocale=gnu \
        --enable-__cxa_atexit \
        --enable-threads=posix \
        --enable-languages=c,c++ \
        --enable-checking=release \
        --with-linker-hash-style=gnu \
        --with-system-zlib && \
    make --silent --jobs=$(nproc) && \
    make --silent --jobs=$(nproc) install-strip && \
    \
    mkdir /gcc-${GCC_VERSION}/build-libstdc++ && \
    cd /gcc-${GCC_VERSION}/build-libstdc++ && \
    ../libstdc++-v3/configure \
        --prefix=/usr/local \
        --build=$(uname -m)-alpine-linux-musl \
        --host=$(uname -m)-alpine-linux-musl \
        --target=$(uname -m)-alpine-linux-musl \
        --disable-libstdcxx-pch \
        --disable-multilib \
        --disable-static \
        --disable-nls \
        --enable-tls \
        --enable-shared \
        --enable-libstdcxx-threads \
        --enable-version-specific-runtime-libs && \
    make --silent --jobs=$(nproc) && \
    make --silent --jobs=$(nproc) install-strip

FROM alpine as builder

COPY --from=gcc-builder /usr/local/ /usr/local/

ARG CMAKE_VERSION=3.15.2

# Download, configure, build and install the CMake
RUN apk add --quiet --no-cache \
        linux-headers \
        build-base \
        make \
        git \
        gcc \
        g++ && \
    git clone \
        --branch v${CMAKE_VERSION} \
        --single-branch \
        --depth 1 \
        https://github.com/Kitware/CMake.git && \
    export CC=/usr/local/bin/gcc && \
    export CXX=/usr/local/bin/g++ && \
    export LIBRARY_PATH=/usr/local/lib64:/usr/local/lib && \
    export LD_LIBRARY_PATH=/usr/local/lib64:/usr/local/lib && \
    cd CMake && \
    ./bootstrap -- -DCMAKE_BUILD_TYPE:STRING=Release && \
    make --jobs=$(nproc) && \
    make --jobs=$(nproc) install && \
    cmake --version

FROM alpine

ARG VERSION
ARG BUILD_DATE
ARG VCS_REF

# Build-time metadata as defined at http://label-schema.org
LABEL \
    maintainer="tymoteusz.blazejczyk.pl@gmail.com" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.build-date=$BUILD_DATE \
    org.label-schema.name="tymonx/gcc" \
    org.label-schema.description="A Docker image with the GNU GCC C/C++ compiler" \
    org.label-schema.usage="https://gitlab.com/tymonx/docker-gcc/README.md" \
    org.label-schema.url="https://gitlab.com/tymonx/docker-gcc" \
    org.label-schema.vcs-url="https://gitlab.com/tymonx/docker-gcc" \
    org.label-schema.vcs-ref=$VCS_REF \
    org.label-schema.vendor="tymonx" \
    org.label-schema.version=$VERSION \
    org.label-schema.docker.cmd=\
"docker run --rm --user $(id -u):$(id -g) --volume $(pwd):$(pwd) --workdir $(pwd) --entrypoint /bin/sh tymonx/gcc"

COPY --from=builder /usr/local/ /usr/local/

ENV \
    CC=/usr/local/bin/gcc \
    CXX=/usr/local/bin/g++ \
    LIBRARY_PATH=/usr/local/lib64:/usr/local/lib \
    LD_LIBRARY_PATH=/usr/local/lib64:/usr/local/lib

RUN apk add --quiet --no-cache \
        musl-dev \
        mpfr-dev \
        binutils \
        bash \
        mpc1 \
        isl \
        gmp && \
    ln -s /usr/local/bin/gcc /usr/local/bin/cc
